/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Script Displaying time and day of last modifies
function showdate()
{
    var lastModif = document.lastModified;
    var dateModif = new Date(lastModif);
    var day = dateModif.getDate();
    if (day < 10)
    {
        day = '0' + day;
    }
    var month = dateModif.getMonth() + 1;
    if (month < 10)
    {
        month = '0' + month;
    }
    var year = dateModif.getFullYear();
    var hours = dateModif.getHours();
    if (hours < 10)
    {
        hours = '0' + hours;
    }
    var minutes = dateModif.getMinutes();
    if (minutes < 10)
    {
        minutes = '0' + minutes;
    }
    var seconds = dateModif.getSeconds();
    if (seconds < 10)
    {
        seconds = '0' + seconds;
    }
    document.getElementById("show-date").innerHTML = (day + "/" + month + "/" + year + " Last Updated at: " + hours + ":" + minutes + ":" + seconds);
    document.getElementById("copyright").innerHTML = "<strong>Copyright © " + year + " BBC.</strong>The BBC is not responsible for<br>the content of external sites. Read more.";
}